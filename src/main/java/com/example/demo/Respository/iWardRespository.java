package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Ward;

public interface iWardRespository extends JpaRepository <Ward , Long>{
    Ward getWardById (int id) ;
}
